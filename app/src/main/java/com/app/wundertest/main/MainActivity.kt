package com.app.wundertest.main

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.app.wundertest.R
import com.app.wundertest.constants.Constants
import com.app.wundertest.utils.FileUtils.Companion.getFileLocation
import com.app.wundertest.fragments.ListViewFragment
import com.app.wundertest.fragments.MapViewFragment
import com.app.wundertest.model.Model
import com.app.wundertest.utils.FileUtils
import com.app.wundertest.webservice.WebApiService
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.ResponseBody
import org.greenrobot.eventbus.EventBus
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File


class MainActivity : AppCompatActivity(), MainView {
    private lateinit var context: Context
    private lateinit var pagerAdapter: MyPagerAdapter

    private val downloadPresenter = FileDownloadPresenter(this, FileDownloadInteractor())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        context = this

        handleToolbar()

        tabLayout.setupWithViewPager(viewpager)

        pagerAdapter = MyPagerAdapter(supportFragmentManager)
        viewpager.adapter = pagerAdapter

        if(!FileUtils.doesFileExists(FileUtils.getFileLocation(context))) {
            downloadPresenter.downloadFile(Constants.FILE_URL)
        }
    }

    private fun handleToolbar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            supportActionBar!!.elevation = 0f
        }
    }

    private fun performLoading() {
        val fileDownloadEvent: Model.FileDownloadEvent = Model.FileDownloadEvent(true)
        EventBus.getDefault().post(fileDownloadEvent)
    }

    inner class MyPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        override fun getItem(position: Int): Fragment {
            return when(position) {
                0 -> {
                    ListViewFragment()
                } else -> {
                    MapViewFragment()
                }
            }
        }

        override fun getCount(): Int {
            return 2
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return when(position) {
                0 -> {
                    context.getString(R.string.title_list)
                } else -> {
                    context.getString(R.string.title_map)
                }
            }
        }
    }

    fun redirectToMap() {
        val tab = tabLayout.getTabAt(1)
        tab!!.select()
    }

    override fun showProgress() {
        progressbar.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progressbar.visibility = View.GONE
    }

    override fun fileDownloaded(responseBody: ResponseBody?) {
        FileUtils.saveFile(context, responseBody!!)
        performLoading()
    }

    override fun fileDownloadError(error: String?) {
        Toast.makeText(context, error, Toast.LENGTH_LONG).show()
    }
}
