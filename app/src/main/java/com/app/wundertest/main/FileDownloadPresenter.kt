package com.app.wundertest.main

import okhttp3.ResponseBody

/**
 * Created by Darshan on 31-07-2018.
 * @author Darshan Parikh (parikhdarshan36@gmail.com)
 */
class FileDownloadPresenter(var mainView: MainView, var fileDownloadInteractor: FileDownloadInteractor)
    : FileDownloadInteractor.OnDownloadFinishedListener {

    fun downloadFile(fileUrl: String) {
        mainView.showProgress()
        fileDownloadInteractor.downloadFile(fileUrl, this)
    }

    override fun fileDownloadError(error: String?) {
        mainView.hideProgress()
        mainView.apply { fileDownloadError(error) }
    }

    override fun fileDownloaded(responseBody: ResponseBody?) {
        mainView.hideProgress()
        mainView.apply { fileDownloaded(responseBody) }
    }
}