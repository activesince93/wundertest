package com.app.wundertest.main

import okhttp3.ResponseBody

/**
 * Created by Darshan on 31-07-2018.
 * @author Darshan Parikh (parikhdarshan36@gmail.com)
 */
interface MainView {
    fun showProgress()
    fun hideProgress()
    fun fileDownloaded(responseBody: ResponseBody?)
    fun fileDownloadError(error: String?)
}