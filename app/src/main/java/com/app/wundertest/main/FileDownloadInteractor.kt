package com.app.wundertest.main

import com.app.wundertest.webservice.WebApiService
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Darshan on 31-07-2018.
 * @author Darshan Parikh (parikhdarshan36@gmail.com)
 */
class FileDownloadInteractor {
    interface OnDownloadFinishedListener {
        fun fileDownloaded(responseBody: ResponseBody?)
        fun fileDownloadError(error: String?)
    }

    fun downloadFile(fileUrl: String, onDownloadFinishedListener: OnDownloadFinishedListener) {
        val webApiService = WebApiService.create()
        val call = webApiService.downloadFile(fileUrl)

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    onDownloadFinishedListener.fileDownloaded(response.body())
                } else {
                    onDownloadFinishedListener.fileDownloadError(response.message())
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                onDownloadFinishedListener.fileDownloadError(t.message)
            }
        })
    }
}