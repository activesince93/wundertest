package com.app.wundertest.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.wundertest.main.MainActivity
import com.app.wundertest.R
import com.app.wundertest.model.Model
import kotlinx.android.synthetic.main.single_row_vehicle.view.*
import org.greenrobot.eventbus.EventBus

/**
 * Created by Darshan on 31-07-2018.
 * @author Darshan Parikh (parikhdarshan36@gmail.com)
 */
class VehicleAdapter(): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val TYPE_ITEM = 0
    }

    private lateinit var vehicleList: List<Model.Vehicle>
    private lateinit var context: Context

    constructor(context: Context, vehicleList: List<Model.Vehicle>) : this() {
        this.context = context
        this.vehicleList = vehicleList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.single_row_vehicle, parent, false)
        return ItemHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(holder is ItemHolder) {
            val vehicle: Model.Vehicle = vehicleList.get(position)
            holder.txtName.text = String.format(context.getString(R.string.name_dynamic), vehicle.name)
            holder.txtAddress.text = String.format(context.getString(R.string.address_dynamic), vehicle.address)
            holder.txtVin.text = String.format(context.getString(R.string.vin_dynamic), vehicle.vin)
            holder.txtFuel.text = String.format(context.getString(R.string.fuel_dynamic), vehicle.fuel)

            holder.btnShowOnMap.setOnClickListener {
                (context as MainActivity).redirectToMap()

                val markerClickedEvent = Model.MarkerClickedEvent(position)
                EventBus.getDefault().post(markerClickedEvent)
            }
        }
    }

    override fun getItemCount(): Int {
        return vehicleList.size
    }

    override fun getItemViewType(position: Int): Int {
        return TYPE_ITEM
    }

    inner class ItemHolder(view: View): RecyclerView.ViewHolder(view) {
        val txtAddress = view.txtAddress
        val txtFuel = view.txtFuel
        val txtName = view.txtName
        val txtVin = view.txtVin
        val btnShowOnMap = view.btnShowOnMap
    }
}