package com.app.wundertest.utils

import android.content.Context
import com.app.wundertest.model.Model
import com.google.gson.Gson
import okhttp3.ResponseBody
import org.json.JSONObject
import java.io.*

/**
 * Created by Darshan on 31-07-2018.
 * @author Darshan Parikh (parikhdarshan36@gmail.com)
 */
class FileUtils {
    companion object {
        fun readFileData(fileLocation: String): List<Model.Vehicle>? {
            try {
                val fis = FileInputStream(fileLocation)
                val dis = DataInputStream(fis)
                val br = BufferedReader(InputStreamReader(dis))
                val strLine: String = br.readLine()

                val jsonObject = JSONObject(strLine)
                val jsonArray = jsonObject.getJSONArray("placemarks")

                val vehiclesList = Gson().fromJson(jsonArray.toString(), Array<Model.Vehicle>::class.java).toList()

                dis.close()
                return vehiclesList
            } catch (e: IOException) {
                e.printStackTrace()
            }
            return null
        }


        fun saveFile(context: Context, body: ResponseBody): Boolean {
            try {
                val filePath = getFileLocation(context)

                var inputStream: InputStream? = null
                var outputStream: OutputStream? = null

                try {
                    val fileReader = ByteArray(4096)

                    var fileSizeDownloaded: Long = 0

                    inputStream = body.byteStream()
                    outputStream = FileOutputStream(filePath)

                    while (true) {
                        val read = inputStream!!.read(fileReader)

                        if (read == -1) {
                            break
                        }

                        outputStream.write(fileReader, 0, read)

                        fileSizeDownloaded += read.toLong()
                    }

                    outputStream.flush()

                    return true
                } catch (e: IOException) {
                    return false
                } finally {
                    inputStream?.close()
                    outputStream?.close()
                }
            } catch (e: IOException) {
                return false
            }

        }

        fun getFileLocation(context: Context): String {
            return (StringBuilder())
                    .append(context.getExternalFilesDir(null))
                    .append(File.separator)
                    .append("locations.json")
                    .toString()
        }

        fun doesFileExists(filePath: String): Boolean {
            val file = File(filePath)
            return file.exists()
        }

    }
}