package com.app.wundertest.webservice

import com.app.wundertest.constants.Constants
import com.app.wundertest.model.Model
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Url


/**
 * Created by Darshan on 15-05-2018.
 * @author Darshan Parikh (parikhdarshan36@gmail.com)
 */

interface WebApiService {

    @GET(Constants.GET_VEHICLES)
    fun getVehicles(): Observable<Model.Response>

    companion object {
        fun create(): WebApiService {
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(Constants.BASE_URL)
                    .build()

            return retrofit.create(WebApiService::class.java)
        }
    }

    @GET
    fun downloadFile(@Url fileUrl: String): Call<ResponseBody>
}