package com.app.wundertest.constants

/**
 * Created by Darshan on 27-07-2018.
 * @author Darshan Parikh (parikhdarshan36@gmail.com)
 */
class Constants {
    companion object {
        //------------- START: Web APIS -------------//
        const val BASE_URL = "https://s3-us-west-2.amazonaws.com/"
        const val GET_VEHICLES = "wunderbucket/locations.json"
        const val FILE_URL = "https://s3-us-west-2.amazonaws.com/wunderbucket/locations.json"
        const val VEHICLE_DATA = "vehicle_data"
        //------------- END: Web APIS -------------//
    }
}