package com.app.wundertest.fragments

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.app.wundertest.R
import com.app.wundertest.constants.Constants
import com.app.wundertest.model.Model
import com.app.wundertest.utils.FileUtils
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.gson.Gson
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class MapViewFragment : Fragment(), OnMapReadyCallback {
    private lateinit var googleMap: GoogleMap
    private lateinit var vehicleList: List<Model.Vehicle>
    private var markerCount = 0

    companion object {
        const val RC_LOCATION_PERMISSION = 1001
    }

    override fun onMapReady(googleMap1: GoogleMap?) {
        googleMap = googleMap1!!
        enableCurrentLocationFeature()

        if(FileUtils.doesFileExists(FileUtils.getFileLocation(context!!))) {
            vehicleList = readFileData()
            putPinsOnMap(vehicleList)
        }

        googleMap.setOnMarkerClickListener(GoogleMap.OnMarkerClickListener { marker ->
            if(marker == null) return@OnMarkerClickListener true

            googleMap.clear()
            if(markerCount == 1) {
                putPinsOnMap(vehicleList)
            } else {
                googleMap.addMarker(MarkerOptions().title(marker.title).position(marker.position)).showInfoWindow()
                markerCount = 1
            }
            true
        })

        googleMap.setOnInfoWindowClickListener {
            googleMap.clear()
            putPinsOnMap(vehicleList)
        }
    }

    private fun enableCurrentLocationFeature() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && ActivityCompat.checkSelfPermission(context!!
                        , Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), RC_LOCATION_PERMISSION)
            return
        }

        googleMap.isMyLocationEnabled = true
        googleMap.uiSettings.isMyLocationButtonEnabled = true

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_map, container, false)

        val mapFragment = childFragmentManager.findFragmentById(R.id.googleMap) as SupportMapFragment?
        mapFragment?.getMapAsync(this)

        retainInstance = true

        return view
    }

    private fun putPinsOnMap(vehicleList: List<Model.Vehicle>) {
        markerCount = vehicleList.size
        if (vehicleList.isEmpty()) return

        vehicleList.forEach {
            addMarkerOnMap(it)
        }
    }

    private fun addMarkerOnMap(vehicle: Model.Vehicle, showInfoWindow: Boolean) {
        val coordinates = vehicle.coordinates
        val latLng = LatLng(coordinates[1], coordinates[0])

        // Add marker
        val marker = googleMap
                .addMarker(MarkerOptions()
                        .position(latLng)
                        .title(vehicle.name))
        if(showInfoWindow) {
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13f))
            marker.showInfoWindow()
        }
    }

    private fun addMarkerOnMap(vehicle: Model.Vehicle) {
        addMarkerOnMap(vehicle, false)
    }

    override fun onStart() {
        EventBus.getDefault().register(this)
        super.onStart()
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onFileDownloaded(fileDownloadEvent: Model.FileDownloadEvent) {
        if(fileDownloadEvent.status) {
            vehicleList = readFileData()
            putPinsOnMap(vehicleList)
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMarkerClicked(markerClickedEvent: Model.MarkerClickedEvent) {
        googleMap.clear()
        val vehicle = vehicleList[markerClickedEvent.position]
        addMarkerOnMap(vehicle, true)
    }

    private fun readFileData(): List<Model.Vehicle> {
        return FileUtils.readFileData(FileUtils.getFileLocation(context!!)) as ArrayList<Model.Vehicle>
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString(Constants.VEHICLE_DATA, Gson().toJson(vehicleList))
        super.onSaveInstanceState(outState)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if(requestCode == RC_LOCATION_PERMISSION
                && grantResults.isNotEmpty()
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            enableCurrentLocationFeature()
        } else {
            Toast.makeText(context, getString(R.string.error_grant_location_permission), Toast.LENGTH_LONG).show()
        }
    }
}