package com.app.wundertest.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.wundertest.constants.Constants
import com.app.wundertest.utils.FileUtils
import com.app.wundertest.R
import com.app.wundertest.adapter.VehicleAdapter
import com.app.wundertest.model.Model
import com.google.gson.Gson
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class ListViewFragment : Fragment() {

    private lateinit var vehicleRecyclerView: RecyclerView
    private lateinit var vehicleList: List<Model.Vehicle>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_list, container, false)

        vehicleRecyclerView = view.findViewById(R.id.vehicleRecyclerView)
        vehicleRecyclerView.layoutManager = LinearLayoutManager(context)

        if(savedInstanceState != null) {
            val vehicleDataStr = savedInstanceState.getString(Constants.VEHICLE_DATA)
            vehicleList = Gson().fromJson(vehicleDataStr, Array<Model.Vehicle>::class.java).toList()
            setRecyclerView(vehicleList)
        } else if(FileUtils.doesFileExists(FileUtils.getFileLocation(context!!))) {
            vehicleList = readFileData()
            setRecyclerView(vehicleList)
        }
        return view
    }

    private fun setRecyclerView(vehicleList: List<Model.Vehicle>) {
        if(vehicleList.isEmpty()) return

        val adapter = VehicleAdapter(context!!, vehicleList)
        vehicleRecyclerView.adapter = adapter
    }

    override fun onStart() {
        EventBus.getDefault().register(this)
        super.onStart()
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onFileDownloaded(fileDownloadEvent: Model.FileDownloadEvent) {
        if(fileDownloadEvent.status) {
            vehicleList = readFileData()
            setRecyclerView(vehicleList)
        }
    }

    private fun readFileData(): List<Model.Vehicle> {
        return FileUtils.readFileData(FileUtils.getFileLocation(context!!)) as ArrayList<Model.Vehicle>
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString(Constants.VEHICLE_DATA, Gson().toJson(vehicleList))
        super.onSaveInstanceState(outState)
    }
}