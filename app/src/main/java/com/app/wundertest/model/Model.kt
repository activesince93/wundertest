package com.app.wundertest.model

/**
 * Created by Darshan on 26-07-2018.
 * @author Darshan Parikh (parikhdarshan36@gmail.com)
 */
object Model {
    data class Vehicle(
            val address: String,
            val coordinates: List<Double>,
            val engineType: String,
            val exterior: String,
            val fuel: Int,
            val interior: String,
            val name: String,
            val vin: String
    )

    data class Response(val vehicleList: List<Vehicle>)
    data class FileDownloadEvent(val status: Boolean)
    data class MarkerClickedEvent(val position: Int)
}